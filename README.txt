INTRODUCTION
-------------
Maintainers:
  Andrew Elster (http://drupal.org/user/282823)

Developed with Astonish Design (http://www.astonishdesign.com)

Licensed under GNU/GPL License

This was developed as an in-house utility for Astonish Design, to avoid sending
email out from the production sever. Instead, this module intercepts mail from
Drupal, by implementing the mail wrapper, and pushes it into a Redis queue.

Use this module in conjunction with Astonish Postmaster (https://astonishdesigns@bitbucket.org/astonishdesigns/astonishpostmaster.git). Postmaster pops mail
off of the Redis queue and sends via mailgun.

INSTALLATION
------------
Dependencies:
  * Redis (http://redis.io)
  * PHPRedis extension (https://github.com/nicolasff/phpredis)
  * Astonish Postmaster (https://astonishdesigns@bitbucket.org/astonishdesigns/astonishpostmaster.git)
      * A Mailgun account (http://www.mailgun.net)

1. Install and enable dependencies
2. Disable any other module that implements the mail wrapper (i.e. queue_mail).
3. Place this module in the usual location (modules/contrib)
4. Enable this module
5. Go to admin/settings/astonish-mail and configure the Redis credentials

At this point all outgoing mail will be pushed into Redis.

********************************************************************************
NOTE: This is ABSOLUTELY USELESS to you if you do not have a coresponding
process to pop mail off the Redis queue and actually email it!
********************************************************************************

One method for sending the email is to install Astonish Postmaster (https://astonishdesigns@bitbucket.org/astonishdesigns/astonishpostmaster.git).
Postmaster is a Node.js application which processes mail from the Redis queue
and transports it via Mailgun (http://www.mailgun.net).
