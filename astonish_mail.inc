<?php

/**
 * Implement drupal_mail_wrapper
 *
 * Insert messages into Redis queue instead of
 * sending directly through SMTP
 *
 * @param  array $message Email array
 * @return string         Status message
 */
function drupal_mail_wrapper($message) {
  $redis = new Redis();
  $host = variable_get('mq_redis_host', 'localhost');
  $port = variable_get('mq_redis_port', 6379);
  $password = variable_get('mq_redis_password', '');

  $redis->connect($host, $port);
  if (!empty($password)) {
    $redis->auth($password);
  }

  // Reformat message
  // Change body to html or text (for mailgun)
  if (strpos($message['headers']['content-type'], 'text/html')) {
    $message['html'] = $message['body'];
  }
  else {
    $message['text'] = $message['body'];
  }

  $status = $redis->rpush('mail', json_encode($message));

  if (false !== $status) {
    return t('Mail successfully pushed onto the Redis queue.');
  }
  else {
    return t('Error: Failed to push message onto the Redis queue.');
  }
}
